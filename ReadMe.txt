==============================================================================

There are multiple approches for achiving the ouput for given feature

I have created console application "GlobalRightsManagementSystem"

I have added following two text files which contains the supplied referece data

1. txtDistributionPartnerContracts.txt - This will contain the list of Distribution Partners.
2. txtMusicContracts.txt - This will contain the list of Music Contracts with Artist.

I have added methods which will read the above files and add into Datatable.

Further I have added methods to filter these Datatable based on User input.


Testing Framework 

I have created new project GlobalRightsManagementSystemTest for writing test cases

1. I have used Microsoft.VisualStudio.UnitTesting Framework to write TDD test


How to run = > Open MusicContractUnitTest.cs file 

Right click and click on Run Tests


2. I have used SpecFlow Framework to write the BDD test

How to run = > open MusicContractFeature.feature file

Right Click Feature: and click on run SpecFlow Scenarios


==============================================================================




