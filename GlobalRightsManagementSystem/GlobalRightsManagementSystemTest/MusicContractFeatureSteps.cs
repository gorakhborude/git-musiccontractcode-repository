﻿using System;
using System.Data;
using System.Linq;
using GlobalRightsManagementSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using TechTalk.SpecFlow;

namespace GlobalRightsManagementSystemTest
{
    [Binding]
    [DeploymentItem("txtDistributionPartnerContracts.txt")]
    [DeploymentItem("txtMusicContracts.txt")]
    public class MusicContractFeatureSteps
    {
        private DataTable dtMusicContract;
        private DataTable dtPartnerContract;
        private DataTable dtResult;
        private RightManagement objRightManagement;

        [BeforeScenario()]
        public void InitializeMusicContractData()
        {
            objRightManagement = new RightManagement();
            dtMusicContract = MusicContractFileReader.GetDataFromFile("txtMusicContracts.txt");
            dtPartnerContract = MusicContractFileReader.GetDataFromFile("txtDistributionPartnerContracts.txt");
        }

        [Given(@"the supplied reference data")]
        public void GivenTheSuppliedReferenceData()
        {

           
        }

        [When(@"user perform enter provider as (.*),(.*)")]
        public void WhenUserPerformEnterProviderAsITunes(string p0, string p1)
        {
            string partner = p0;
            string effectivedate = p1;
            dtResult = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract,partner, effectivedate);
        }

        [Then(@"the output should be")]
        public void ThenTheOutputShouldBe(Table table)
        {
            foreach (DataRow drResult in dtResult.Rows)
            {
                string title = drResult["Title"].ToString();
                string artist = drResult["Artist"].ToString();
                string usages = drResult["Usages"].ToString();

                var data = from c in table.Rows
                    where String.Equals(c["Title"], title, StringComparison.OrdinalIgnoreCase) &&
                          String.Equals(c["Artist"], artist, StringComparison.OrdinalIgnoreCase) &&
                          usages.Contains(c["Usages"]) 
                          select c;


                Assert.IsTrue(data.Any());
                
            }
            
            Assert.IsTrue(dtResult.Rows.Count == table.RowCount);
        }
    }
}
