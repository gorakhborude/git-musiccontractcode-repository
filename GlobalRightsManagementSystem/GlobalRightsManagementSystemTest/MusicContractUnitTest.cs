﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GlobalRightsManagementSystem;
namespace GlobalRightsManagementSystemTest
{
    [DeploymentItem("txtDistributionPartnerContracts.txt")]
    [DeploymentItem("txtMusicContracts.txt")]
    [TestClass]
    public class MusicContractUnitTest
    {
        private DataTable dtMusicContract;
        private DataTable dtPartnerContract;
        private RightManagement objRightManagement;

        [TestInitialize]
        public void InitializeMusicContractData()
        {
            objRightManagement = new RightManagement();
            dtMusicContract = MusicContractFileReader.GetDataFromFile("txtMusicContracts.txt");
            dtPartnerContract = MusicContractFileReader.GetDataFromFile("txtDistributionPartnerContracts.txt");
        }

        [TestMethod]
        public void TestInvalidPartnerName()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "Apple", "02-01-2012");
            Assert.IsTrue(result.Rows.Count == 0);
        }

        [TestMethod]
        public void TestActiveContractsSenario1()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "Itunes", "03-01-2012");
            Assert.IsTrue(result.Rows.Count > 0);
            
        }

        [TestMethod]
        public void TestActiveContractsHavingEndDateSenario2()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "YouTube", "12-27-2012");
            Assert.IsTrue(result.Rows.Count > 0);

            result.DefaultView.RowFilter = "Title ='Christmas Special'";
            Assert.IsTrue(result.DefaultView.Count == 1);
        }

        [TestMethod]
        public void TestActiveContractsSenario3()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "YouTube", "04-01-2012");
            Assert.IsTrue(result.Rows.Count > 0);
        }

        [TestMethod]
        public void TestInActiveContractsHavingEndDateSenario4()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "YouTube", "01-01-2013");
            Assert.IsTrue(result.Rows.Count > 0);

            result.DefaultView.RowFilter = "Title ='Christmas Special'";
            Assert.IsTrue(result.DefaultView.Count == 0);
        }

        [TestMethod]
        public void TestInActiveContractsHavingNoEndDateSenario5()
        {
            var result = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, "Itunes", "01-01-2010");
            Assert.IsTrue(result.Rows.Count == 0);
        }
    }
}
