﻿using System;
using System.Data;
using System.Text;

namespace GlobalRightsManagementSystem
{
    public class RightManagement
    {

        public DataTable GetActiveMusicContract(DataTable dtMusicContract, DataTable dtDistributionParter, string deliveryPartnerName, string effectiveDate)
        {
            dtMusicContract.CaseSensitive = false;
            dtDistributionParter.CaseSensitive = false;

            StringBuilder strSQL = new StringBuilder();

            dtDistributionParter.DefaultView.RowFilter = "Partner ='" + deliveryPartnerName + "'";

            if (dtDistributionParter.DefaultView.Count > 0)
            {
                if (dtDistributionParter.DefaultView[0]["Usage"] != null)
                {
                    string strUsage = dtDistributionParter.DefaultView[0]["Usage"].ToString();

                    foreach (var usage in strUsage.Split(',')) // There is possibility to have partmners who supports straming as well as download
                    {
                        if (strSQL.Length > 0)
                            strSQL.Append(" AND ");
                        strSQL.Append(" ((Usages LIKE '%" + usage + "%')");

                    }

                    strSQL.Append(" AND (StartDate <= '" + effectiveDate + "' AND ISNULL(EndDate,'" + effectiveDate + "') >= '" + effectiveDate + "'))");

                }
                dtMusicContract.DefaultView.RowFilter = strSQL.ToString();
            }
            else
            {
                Console.WriteLine("Invalid Partner Name");
                return new DataTable();
            }

            return dtMusicContract.DefaultView.ToTable();
        }

        public string WriteActiveMusicContracts(DataTable dtResult)
        {
            StringBuilder strBuilder = new StringBuilder();
            int i = 0;
            foreach (var colName in dtResult.Columns)
            {
                strBuilder.Append(colName);
                strBuilder.Append("|");
            }

            foreach (DataRow drContract in dtResult.Rows)
            {
                strBuilder.AppendLine();

                foreach (var colValue in drContract.ItemArray)
                {
                    strBuilder.Append(colValue);
                    strBuilder.Append("|");
                }
                
            }

            return strBuilder.ToString();
        }
    }


}
