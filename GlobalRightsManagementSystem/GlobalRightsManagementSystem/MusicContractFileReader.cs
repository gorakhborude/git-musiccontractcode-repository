﻿using System;
using System.Data;
using System.IO;


namespace GlobalRightsManagementSystem
{
    public class MusicContractFileReader
    {

        public static DataTable GetDataFromFile(string filePath)
        {
            int j = 1;
            DateTime dtContractDate;
            DataTable dtContractData = new DataTable("dtContractData");

            dtContractData.Columns.Add("SRNO", typeof(int));
            dtContractData.Columns["SRNO"].AutoIncrement = true;
            dtContractData.Columns["SRNO"].AutoIncrementSeed = 1;
            dtContractData.Columns["SRNO"].AutoIncrementStep = 1;

            string[] lines = File.ReadAllLines(filePath);

            for (int i = 0; i < lines.Length; i++)
            {
                if (i == 0) //read columns
                {
                    foreach (var strColName in lines[i].Split('|'))
                    {
                        if (strColName == "StartDate" || strColName == "EndDate")
                        {
                            dtContractData.Columns.Add(strColName, typeof(DateTime));
                        }
                        else
                        {
                            dtContractData.Columns.Add(strColName, typeof(string));
                        }
                    }
                }
                else
                {
                    DataRow drItem = dtContractData.NewRow();

                    foreach (var strColValue in lines[i].Split('|'))
                    {
                        if (!string.IsNullOrEmpty(strColValue))
                        {
                            drItem[j] = strColValue;
                        }
                        else
                        {
                            drItem[j] = DBNull.Value;
                        }

                        j++;
                    }
                    j = 1;

                    dtContractData.Rows.Add(drItem);

                }
            }

            return dtContractData;
        }
    }
}
