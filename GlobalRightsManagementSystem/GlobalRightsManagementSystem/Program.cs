﻿using System;
using System.IO;

namespace GlobalRightsManagementSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter Partner Name :");
            string partnerName = Console.ReadLine();

            //if (string.IsNullOrEmpty(partnerName)) ;
            // throw new ArgumentException("Invalid Input Arguments");

            Console.WriteLine("please enter effective date :");
            string effectivedate = Console.ReadLine();

            //if (string.IsNullOrWhiteSpace(effectiveDate))
            //    throw new ArgumentException("Invalid Input Arguments");



            string txtMusicContractPath = Path.Combine(Environment.CurrentDirectory, "txtMusicContracts.txt");

            var dtMusicContract = MusicContractFileReader.GetDataFromFile(txtMusicContractPath);

            string txtPatnerContractPath = Path.Combine(Environment.CurrentDirectory, "txtDistributionPartnerContracts.txt");

            var dtPartnerContract = MusicContractFileReader.GetDataFromFile(txtPatnerContractPath);

            RightManagement objRightManagement = new RightManagement();

            var dtResult = objRightManagement.GetActiveMusicContract(dtMusicContract, dtPartnerContract, partnerName,
                effectivedate);

            var result = objRightManagement.WriteActiveMusicContracts(dtResult);
            if (!String.IsNullOrEmpty(result))
                Console.WriteLine(result);
            else
                Console.WriteLine("There is no Active Contracts available for Partner {0} and Effective Date {1}",partnerName,effectivedate);

            Console.ReadLine();
        }


    }
}
